from setuptools import setup

setup(
    name="dynamic_dns",
    version="0.1",
    url="https://github.com/shouptech/dynamic_dns",
    author="Emma Shoup",
    author_email="emma@shoup.io",
    description="A script to create/update a DNS A record in with your current public IP address.",
    install_requires=[
        "requests",
    ],
    entry_points={
        "console_scripts": [
            "dynamic-dns = dynamic_dns.command:main",
        ]
    },
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
)
